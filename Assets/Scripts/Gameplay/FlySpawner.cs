﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class FlySpawner : MonoBehaviour
{
    [SerializeField] private float spawnZoneXBorder;
    [SerializeField] private float spawnZoneYBorder;

    public float spawnCoolDown = 3.0f;
    public int spawnLimit = 2;

    public GameObject flyProto;
    
    void Start()
    {
        StartCoroutine(SpawnRoutine());
    }

    private IEnumerator SpawnRoutine()
    {
        var wait = new WaitForSeconds(spawnCoolDown);
        while (true)
        {
            yield return wait;
            SpawnFly();
        }
    }

    private Vector3 GetSpawnPos()
    {
        return new Vector3(Random.Range(-spawnZoneXBorder, spawnZoneXBorder), Random.Range(-spawnZoneYBorder, spawnZoneYBorder), 0);
    }

    public void SpawnFly()
    {
        Instantiate(flyProto, GetSpawnPos(), Quaternion.identity);
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(transform.position + new Vector3(-spawnZoneXBorder, -spawnZoneYBorder, 0), transform.position + new Vector3(-spawnZoneXBorder, spawnZoneYBorder, 0));
        Gizmos.DrawLine(transform.position + new Vector3(-spawnZoneXBorder, -spawnZoneYBorder, 0), transform.position + new Vector3(spawnZoneXBorder, -spawnZoneYBorder, 0));
        Gizmos.DrawLine(transform.position + new Vector3(-spawnZoneXBorder, spawnZoneYBorder, 0), transform.position + new Vector3(spawnZoneXBorder, spawnZoneYBorder, 0));
        Gizmos.DrawLine(transform.position + new Vector3(spawnZoneXBorder, -spawnZoneYBorder, 0), transform.position + new Vector3(spawnZoneXBorder, spawnZoneYBorder, 0));
    }
}
