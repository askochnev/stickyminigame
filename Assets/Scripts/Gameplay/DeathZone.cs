﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathZone : MonoBehaviour
{
    [SerializeField] private Transform respawn;
    [SerializeField] private GameObject fx;

    private void OnTriggerEnter2D(Collider2D other)
    {
        var character = other.GetComponent<Killable>();
        if (character != null)
        {
            Kill(character);
        }
    }

    private void Kill(Killable target)
    {
        SpawnFX(target.transform);
        target.transform.position = respawn.position;
        target.OnKill();
    }

    private void SpawnFX(Transform targetTransform)
    {
        StartCoroutine(SpawnFXRoutine(targetTransform));
    }

    private IEnumerator SpawnFXRoutine(Transform targetTransform)
    {
        var pos = targetTransform.position;
        pos.z = -2;
        var fxInstance = Instantiate(fx, pos, Quaternion.identity);
        var anim = fxInstance.GetComponent<Animator>();
        yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length);
        Destroy(fxInstance);
    }
}
