﻿using System;
using UnityEngine;

public class Collectable : MonoBehaviour
{
    public Action onCollect;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<StickyCharacter>() != null)
        {
            onCollect();
        }
    }

    public virtual void OnCollect()
    {
         onCollect?.Invoke();
    }
}
