﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickyCharacter : Killable
{
    [SerializeField] private Rigidbody2D rigid;
    [SerializeField] private Animator anim;

    [SerializeField] private float jumpforceHorizontal = 5.0f;
    [SerializeField] private float jumpforceVertical = 3.0f;

    [SerializeField] private bool jumpLeft = false;
    
    public void Jump()
    {
        if (MayJump())
        {
            if (jumpLeft)
            {
                JumpToLeft();
            }
            else
            {
                JumpToRight();
            }
            anim.SetTrigger("Jump");
            anim.ResetTrigger("Fall");
            jumpLeft = !jumpLeft;
        }
    }

    public override void OnKill()
    {
        base.OnKill();
        rigid.velocity = Vector2.zero;
        jumpLeft = false;
        FixOnTheWall();
        var scale = transform.localScale;
        scale.x = Mathf.Abs(scale.x);
        transform.localScale = scale;
    }
    
    #region implementation
    private void JumpToLeft()
    {
        Debug.Log("JumpToLeft");
        SetScale(1);
        rigid.constraints = FlyConstraints();
        rigid.velocity = new Vector2(-jumpforceHorizontal, jumpforceVertical);
    }

    private void JumpToRight()
    {
        Debug.Log("JumpToRight");
        SetScale(-1);
        rigid.constraints = FlyConstraints();
        rigid.velocity = new Vector2(jumpforceHorizontal, jumpforceVertical);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        switch (other.gameObject.tag)
        {
            case "Wall":
                FixOnTheWall();
                break;
            case "Collectable":
                break;
        }
    }

    private void FixOnTheWall()
    {
        rigid.constraints = WallConstraints();
        anim.SetTrigger("Fall");
        anim.ResetTrigger("Jump");
    }
    
    private bool MayJump()
    {
        return rigid.constraints == WallConstraints();
    }

    private RigidbodyConstraints2D FlyConstraints()
    {
        return RigidbodyConstraints2D.FreezeRotation;
    }
    
    private RigidbodyConstraints2D WallConstraints()
    {
        return RigidbodyConstraints2D.FreezeRotation & RigidbodyConstraints2D.FreezePositionX;
    }

    private void SetScale(int sign)
    {
        var scale = transform.localScale;
        scale.x = Mathf.Sign(sign) * Mathf.Abs(scale.x);
        transform.localScale = scale;
    }
    #endregion
}
