﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Killable : MonoBehaviour
{
    public Action onKilled;

    public virtual void OnKill()
    {
        onKilled?.Invoke();
    }
}
