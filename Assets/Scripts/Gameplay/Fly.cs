﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fly : Collectable
{
    private void Start()
    {
        var hud = FindObjectOfType<HUD>();
        onCollect += () =>
        {
            hud.AddFlyToCounter();
            Destroy(this.gameObject);
        };
    }

}
