﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickyCharacterKeyboardController : MonoBehaviour
{
    [SerializeField] private StickyCharacter character;
    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            character.Jump();
        }
    }
}
