﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HUD : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI deathCounter;
    [SerializeField] private TextMeshProUGUI flyCounter;
    
    [SerializeField] private StickyCharacter sticky;

    private int _death;
    private int _flies;
    
    void Start()
    {
        deathCounter.text = "Die: 0";
        _death = 0;
        _flies = 0;
        if (sticky == null)
        {
            sticky = FindObjectOfType<StickyCharacter>();
        }
        sticky.onKilled += AddDeathToCounter;
    }
   
    public void AddFlyToCounter()
    {
        _flies++;
        flyCounter.text = "Fly: " + _flies;
    }

    public void AddDeathToCounter()
    {
        _death++;
        deathCounter.text = "Die: " + _death;
    }
}
